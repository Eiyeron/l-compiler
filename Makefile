CC = gcc

SRCDIR=src
INCDIR=include
OBJDIR=obj

LIBS = -lm
CCFLAGS = -Wall -ggdb -std=c99
EXTRA_CCFLAGS = -fdiagnostics-color=auto


TESTDIR = test

PROGRAM = l

# The rest is to not be touched, thanks.



MODULES = $(wildcard $(SRCDIR)/*.c)
OBJS := $(addprefix $(OBJDIR)/,$(notdir $(MODULES:.c=.o)))

$(PROGRAM): $(OBJS)
	$(CC) $(CCFLAGS) $(EXTRA_CCFLAGS) -I$(INCDIR) -o $@ $^

$(OBJDIR)/%.o: $(SRCDIR)/%.c dir
	$(CC) $(CCFLAGS) $(EXTRA_CCFLAGS) -I$(INCDIR) -o $@ -c $<

$(TESTDIR)/%.l: $(PROGRAM)
	./$(PROGRAM) $@ | diff - $(@:%.l=%.synt)



dir:
	mkdir -p $(OBJDIR)

all: $(PROGRAM)

test:  $(TESTDIR)/*.l

clean:
	- rm -r $(OBJDIR)
	- rm -f $(PROGRAM)

.PHONY : all test
