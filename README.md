#L Language Compiler
## By Florian Arnoux-Dormont (L3 Info AMU 2014-2015)


THis project is a school project which is building a compiler for a C/Lua-styled language called L.

Building instructions:
```bash
make
```

### Global TODO list
- Finish Token Parser (I think it's feature-complete but I'm not sure about it).
- Making options to split the compilation process to allow better debugbing process (for instance ```-t``` to do and print the token parsing).
- Code cleaning
- Commenting
- Wait for the school work to continue
- Codenames for parts? :p

### Makefile
The Makefile allow a little customisation and is a hand-made one. I had first in mind templates I found but each time it has a compilation error, deleting the dependancies files are quite the pain to do.

### Token Parser

The parser takes a file in argument and return to stdout the token listing.

#### Notes about the token parser

- I created a negative TOKEN to tore the unexpected TOKENS which aren't part of any reserved keyword list nor symbol. So the compiler could just stop there as it just found a token error.
- I edited a lot the given code to make it cleaner and more modulable. I have some remarks to do about it:
	- The biggest change is probably the token list which became a X-Macro based file, which allows better readibity and flexibilty (Reflexion trick : with this file I have the capacity to generate the string name of the enumeration). There is a twist : values should each time grow by one (to be aligned with the refletive string list) and negatives should be put at the end. ore thinking should be done about this problem but I don't find it too invasive for now to make me worrry about the future.
	- mangeEspace function : I added the '\r' (carriage return) for Windows target (I can't switch to Linux for now.)
	- Formatting.

#### What should be done

- Further code cleaning.
- Changing the french comments into english.
- Find about what should be done.

#### Integration Test
This project supports a simplistic file/result integration test. It only runs the compiled program and compares the parser result to the given file listing and stops if any test yields different result. For now ad the project only is a token parser, the command to test it is just
```bash
make test
```

### Licence
As it's a school work, the project and all its products (listings, binaries and all the bell and whistle) belongs to a "all right reserved licence" until further licence modification. As a coder, you're allowed to take a look at the code but don't steal it, it's heavily WIP, depends on (possibly) crazy tricks and it's still a **school work**. Don't be a dirty cheater, pal!