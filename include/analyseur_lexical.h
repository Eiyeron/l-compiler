#ifndef __LIRE_UNITE__
#define __LIRE_UNITE__
#include <stdio.h>


#define YYTEXT_MAX 100

extern char yytext[YYTEXT_MAX];
extern int trace_xml;


int yylex(void);
void test_yylex_internal( FILE *yyin );

#endif
