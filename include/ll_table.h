#ifndef L_TABLE
#define L_TABLE

#include "symboles.h"

int ll_table[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];

void initialise_ll_table(void);

int is_ll_transition(int non_terminal, int terminal);

#endif