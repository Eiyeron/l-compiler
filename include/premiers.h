#ifndef __PREMIERS__
#define __PREMIERS__

#include"symboles.h"

int premiers[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];


void initialise_premiers(void);
int est_premier(int non_terminal, int terminal);

#define MERGE_PREMIERS(ntA, ntB) if(!premiers[ntA][i] && premiers[ntB][i]) {premiers[ntA][i] = premiers[ntB][i]; hasChanged = true; }
#define MERGE_PREMIEA2(ntA, ntB, nTc) if(premiers[ntB][EPSILON]) {MERGE_PREMIERS(ntA, ntB);}
#define SET_EPSILON(ntA, ntB) if(premiers[ntB][EPSILON]) premiers[ntA][EPSILON] = i;

#endif
