#ifndef SYMBOLES_H
#define SYMBOLES_H

/* symboles non terminaux */

#define NB_NON_TERMINAUX 39

enum symboles_non_terminaux {
#define X(a) a,
#include "symboles_non_terminaux.def"
#undef X
};

/* symboles terminaux */
#define NB_TERMINAUX 33
#define NB_ERREURS 1

enum symboles_terminaux {
#define X(a) a,
#define X_V(a, b) a=b,
#include "symboles_terminaux.def"
#undef X
#undef X_V
};

extern const char *symboles_non_terminaux_name[];
extern const char *symboles_terminaux_name[];

#endif