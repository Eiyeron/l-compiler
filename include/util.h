#ifndef __UTIL__
#define __UTIL__

char *duplique_chaine(char *s);
void erreur(const char *message, const int ligne);
void affiche_balise_ouvrante(const char *fct_, int trace_xml);
void affiche_balise_fermante(const char *fct_, int trace_xml);
void affiche_element(char *fct_, char *texte_, int trace_xml);
void affiche_unite_element(int unite);
void affiche_texte(char *texte_, int trace_xml);
void nom_token(int token, char *nom, char *valeur);
#endif
