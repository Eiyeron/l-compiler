#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "symboles.h"
#include "analyseur_lexical.h"
#include "util.h"

#define is_num(c)(('0' <= (c)) && ((c) <= '9'))
#define is_maj(c)(('A' <= (c)) && ((c) <= 'Z'))
#define is_min(c)(('a' <= (c)) && ((c) <= 'z'))
#define is_alpha(c)(is_maj(c) || is_min(c) || (c) == '_' || (c) == '$')
#define is_alphanum(c)(is_num((c)) || is_alpha((c)))

extern FILE *yyin;

char yytext[YYTEXT_MAX];
int yyleng;
int yylval;
int nbMotsClefs = 9;
/* Compter les lignes pour afficher les messages d'erreur avec numero ligne */
int nb_ligne = 1;

// Static functions prototypes
static int isYytextEqualsTo(const char* keyword);
static int lireCar(void);
static enum symboles_terminaux getBlockSymbolCode(char symbol);
static enum symboles_terminaux get_symbol_code(char symbol);
static enum symboles_terminaux check_for_keywords();
static enum symboles_terminaux go_into_alphanum_read();
static enum symboles_terminaux go_into_num_read();


/*******************************************************************************
* Fonction auxiliaire appelée par le compilo en mode -l, pour tester l'analyseur
* lexical et, étant donné un programme en entrée, afficher la liste des tokens.
******************************************************************************/

void test_yylex_internal(FILE *yyin)
{
    int uniteCourante;
    char nom[100];
    char valeur[100];
    uniteCourante = yylex();
    while (uniteCourante != FIN)
    {
        nom_token( uniteCourante, nom, valeur );
        printf("%s\t%s\t%s\n", yytext, nom, valeur);
        uniteCourante = yylex();
    }
    nom_token( uniteCourante, nom, valeur );
    printf("%s\t%s\t%s\n", yytext, nom, valeur);
}


//////////////////////////////////////////////////////////////////////////////
// STATIC FUNCTIONS
//////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
* Fonction qui ignore les espaces et commentaires.
* Renvoie UNEXPECTED_TOKEN si arrivé à la fin du fichier, 0 si tout va bien
******************************************************************************/
static int mangeEspaces()
{
    char c = fgetc(yyin);
    int comment = 0;
    while( comment || (c == ' ') || (c == '\n') || (c == '\t') || (c == '\r') || (c == '#' ) )
    {
        if( c == '#' )
        {
            comment = 1;
        }

        if( c == '\n' )
        {
            nb_ligne++;
            comment = 0;
        }

        c = fgetc(yyin);
    }

    if ( feof(yyin) )
    {
        return UNEXPECTED_TOKEN;
    }

    ungetc(c, yyin);
    return 0;
}

/*******************************************************************************
* Lit un caractère et le stocke dans le buffer yytext
* @return the read character.
******************************************************************************/
static int lireCar(void)
{
    yytext[yyleng++] = fgetc(yyin);
    yytext[yyleng] = '\0';
    return yytext[yyleng - 1];
}

/*******************************************************************************
* Remet le dernier caractère lu au buffer clavier et enlève du buffer yytext
******************************************************************************/
static void delireCar()
{
    char c;
    c = yytext[yyleng - 1];
    yytext[--yyleng] = '\0';
    ungetc(c, yyin);
}

/*******************************************************************************
* Fonction principale de l'analyseur lexical, lit les caractères de yyin et
* renvoie les tokens sous forme d'entier. Le code de chaque unité est défini
* dans symboles.h sinon (mot clé, idententifiant, etc.). Pour les tokens de
* type ID_FCT, ID_VAR et NOMBRE la valeur du token est dans yytext, visible
* dans l'analyseur syntaxique.
******************************************************************************/
int yylex(void)
{
    char charRead;
    int endOfFile;
    yytext[yyleng = 0] = '\0';
    // reset yytext buffer

    endOfFile = mangeEspaces();
    if (endOfFile)
    {
        yytext[0] = '.';
        yyleng = 1;
        yytext[yyleng] = '\0';
        return FIN;
    }


    mangeEspaces();
    charRead = lireCar();
    int code = get_symbol_code(charRead);
    if (code != UNEXPECTED_TOKEN)
    {
        return code;
    }

    if(charRead == '$')
    {
// The special case with that character is that it begins the variable parsing.
// So, as we know that's a variable, we just have to parse an alphanumeric name.
        go_into_alphanum_read();
        return ID_VAR;
    }

    if(is_alphanum(charRead))
    {
        if(is_num(charRead))
        {
// This is mainly to avoid parsing numbers as alphabetic tokens.
            return go_into_num_read();
        }
        return go_into_alphanum_read();
    }

    return UNEXPECTED_TOKEN;
}

/*******************************************************************************
* This function assumes that the keyword and yytext have a null terminator
* Thus, to check that yytext is a keyword, it should be equal to it up to
* the null character.
* @return 1 if yytext has the same content than the keyword, else 0
******************************************************************************/
static int isYytextEqualsTo(const char* keyword)
{
    for (int i = 0; i <= strlen(keyword); ++i)
    {
// the int cast removes a curious warning
        if(tolower((int)yytext[i]) != keyword[i])
            return 0;
    }
    return 1;
}

/*******************************************************************************
* Tries to parse a 2-wide symbol block symbol. rewinds the last character if
* it didn't found a block symbol.
* @param  symbol the starting character.
* @return the token ID. UNEXPECTED_TOKEN if not found?
******************************************************************************/
static enum symboles_terminaux getBlockSymbolCode(char symbol)
{
    const char blockPrefixes[] = "!<=";

    const char *blocSymbols[3] = {
        "!=",
        "<=",
        "=="
    };

    if(strchr(blockPrefixes, symbol) == NULL)
    {
        return UNEXPECTED_TOKEN;
    }

    lireCar();
    for (int i = 0; i < 3; ++i)
    {
        if(isYytextEqualsTo(blocSymbols[i]))
        {
            return DIFFERENT_DE + i;
        }
    }

    delireCar();
    return UNEXPECTED_TOKEN;
}

/*******************************************************************************
* Parse a special character and call the block parser if the symbol is a block prefix.
* @param symbol the character to parse or the block prefix.
* @return the token ID.
******************************************************************************/
static enum symboles_terminaux get_symbol_code(char symbol)
{
    if(symbol == ',')
        return VIRGULE;

    int blockSymbol = getBlockSymbolCode(symbol);
    if(blockSymbol != UNEXPECTED_TOKEN)
    {
        return blockSymbol;
    }

const char* symbolArray = ";+-*/()[]{}=<&|!"; // same order as in symboles.h
    char* matchedSymbolPointer = strchr(symbolArray, symbol);
    if (matchedSymbolPointer == 0) return UNEXPECTED_TOKEN;

    int index = matchedSymbolPointer - symbolArray;
    int first_symbol = POINT_VIRGULE;
return first_symbol + index; // codes are consecutive
}


/*******************************************************************************
* Checks if yytext belongs to the reserved keywords.
* @return the token ID or UNEXPECTED_TOKEN if it's not reserved.
******************************************************************************/
static enum symboles_terminaux check_for_keywords()
{
// TODO : use array for keywords
    char *keywords[8] = {
        "si",
        "alors",
        "sinon",
        "tantque",
        "faire",
        "entier",
        "retour",
        "pour"
    };
    for (int i = 0; i < 8; ++i)
    {
        if(isYytextEqualsTo(keywords[i]))
            return SI + i;
    }
    return UNEXPECTED_TOKEN;
}

/*******************************************************************************
* Parse a function.
* @return The token ID.
******************************************************************************/
static enum symboles_terminaux check_for_builtins()
{
    char *builtins[2] = {
        "lire",
        "ecrire"
    };

    for (int i = 0; i < 2; ++i)
    {
        if(isYytextEqualsTo(builtins[i])) {
            return LIRE + i;
        }
    }
    return ID_FCT;
}

/*******************************************************************************
* Parse an alphanumeric token.
* @return The token ID.
******************************************************************************/
static enum symboles_terminaux go_into_alphanum_read()
{
    char charRead;
    do
    {
        charRead = lireCar();
    } while(is_alphanum(charRead));
    // check for functions and keywords
    delireCar();
    if(check_for_keywords() == UNEXPECTED_TOKEN)
    {
        mangeEspaces();
        // if it's a function definition or a function call.
        return check_for_builtins();
    }
    return check_for_keywords();
}

/*******************************************************************************
* Parse a numeric token.
* @return The token ID, NOMBRE.
******************************************************************************/
static enum symboles_terminaux go_into_num_read()
{
    char charRead;
    do
    {
        charRead = lireCar();
    } while(is_num(charRead));
    delireCar();
    return NOMBRE;
}