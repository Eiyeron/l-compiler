#include "analyseur_syntaxique.h"
#include "affiche_arbre_abstrait.h"
#include "analyseur_lexical.h"
#include "premiers.h"
#include "suivants.h"
#include "ll_table.h"
#include "symboles.h"
#include "util.h"

#include <string.h>
#include <stdlib.h>

int uniteCourante;
int trace_xml = 1;

void check_syntax_error(int code)
{
	if (uniteCourante != code) {
		erreur("Syntax ERROR", code);
	}
}

void show_and_consume()
{
	affiche_unite_element(uniteCourante);
	uniteCourante = yylex();

}

n_prog* programme()
{
	n_l_dec *opt_dec_variables  = NULL;
	n_l_dec *list_dec_fonctions = NULL;
	n_prog  *programme          = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	opt_dec_variables   = optDecVariables();
	list_dec_fonctions  = listeDecFonctions();
	programme           = cree_n_prog(opt_dec_variables, list_dec_fonctions);
	affiche_balise_fermante(__func__, trace_xml);
	return programme;
}

n_l_dec* optDecVariables()
{
	n_l_dec *opt_dec_variables   = NULL;
	n_l_dec *liste_dec_variables = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_listeDecVariables_, uniteCourante)) {

		liste_dec_variables = listeDecVariables();

		check_syntax_error(POINT_VIRGULE);
		show_and_consume();

		opt_dec_variables = cree_n_l_dec(liste_dec_variables->tete, liste_dec_variables->queue);
		affiche_balise_fermante(__func__, trace_xml);
		return opt_dec_variables;
	}

	if (est_suivant(_optDecVariables_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;

}

n_l_dec* listeDecVariables()
{
	n_dec   *declaration_variable              = NULL;
	n_l_dec *liste_declaration_variables_bis   = NULL;
	n_l_dec *liste_declaration_variable_concat = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_declarationVariable_, uniteCourante)) {

		declaration_variable              = declarationVariable();
		liste_declaration_variables_bis   = listeDecVariablesBis();
		liste_declaration_variable_concat = cree_n_l_dec(declaration_variable, liste_declaration_variables_bis);

		affiche_balise_fermante(__func__, trace_xml);
		return liste_declaration_variable_concat;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_l_dec* listeDecVariablesBis()
{
	n_l_dec *liste_dec_variables_result = NULL;
	n_l_dec *liste_dec_variables_bis = NULL;
	n_dec *declaration_variable = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == VIRGULE) {
		show_and_consume();

		if (est_premier(_declarationVariable_, uniteCourante)) {

			declaration_variable = declarationVariable();
			liste_dec_variables_bis = listeDecVariablesBis();
			liste_dec_variables_result = cree_n_l_dec(declaration_variable, liste_dec_variables_bis);

			return liste_dec_variables_result;
		}
	}

	if (est_suivant(_listeDecVariablesBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, __LINE__);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_dec* declarationVariable()
{
	n_dec* declaration_variable = NULL;
	char  nom_variable[YYTEXT_MAX];
	int    opt_taille_tableau;
	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(ENTIER);
	show_and_consume();

	check_syntax_error(ID_VAR);
	strcpy(nom_variable, yytext);
	show_and_consume();

	opt_taille_tableau = optTailleTableau();
	if (opt_taille_tableau < 0)
		declaration_variable = cree_n_dec_var(nom_variable);
	else
		declaration_variable = cree_n_dec_tab(nom_variable, opt_taille_tableau);

	affiche_balise_fermante(__func__, trace_xml);
	return declaration_variable;
}

int optTailleTableau()
{
	int dimension_tableau;

	affiche_balise_ouvrante(__func__, trace_xml);

	if (uniteCourante == CROCHET_OUVRANT) {
		show_and_consume();
		check_syntax_error(NOMBRE);
		dimension_tableau = atoi(yytext);
		show_and_consume();

		check_syntax_error(CROCHET_FERMANT);
		show_and_consume();

		return dimension_tableau;
	}
	if (est_suivant(_optTailleTableau_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return -1;
	}
	erreur(__func__, __LINE__);
	return -1;
}

n_l_dec* listeDecFonctions()
{
	n_l_dec *liste_dec_fonction_result = NULL;
	n_dec *declaration_fonction = NULL;
	n_l_dec *liste_dec_fonctions = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);

	if (est_premier(_declarationFonction_, uniteCourante)) {
		declaration_fonction = declarationFonction();
		liste_dec_fonctions = listeDecFonctions();
		liste_dec_fonction_result = cree_n_l_dec(declaration_fonction, liste_dec_fonctions);
		affiche_balise_fermante(__func__, trace_xml);
		return liste_dec_fonction_result;
	}
	if (est_suivant(_listeDecFonctions_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_dec* declarationFonction()
{
	n_dec   *declaration_fonction = NULL;
	n_l_dec *liste_param          = NULL;
	n_l_dec *opt_dec_variables    = NULL;
	n_instr *instruction_bloc     = NULL;
	char nom_fonction[YYTEXT_MAX];

	affiche_balise_ouvrante(__func__, trace_xml);
	strcpy(nom_fonction, yytext);
	check_syntax_error(ID_FCT);
	show_and_consume();

	if (est_premier(_listeParam_, uniteCourante)) {
		liste_param = listeParam();
		opt_dec_variables = optDecVariables();
		if (est_premier(_instructionBloc_, uniteCourante)) {
			instruction_bloc =  instructionBloc();
			declaration_fonction = cree_n_dec_fonc(nom_fonction, liste_param, opt_dec_variables, instruction_bloc);

			affiche_balise_fermante(__func__, trace_xml);
			return declaration_fonction;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_l_dec* listeParam()
{
	n_l_dec *liste_param       = NULL;
	n_l_dec *opt_dec_variables = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(PARENTHESE_OUVRANTE);
	show_and_consume();
	opt_dec_variables = optListeDecVariables();
	if (opt_dec_variables != NULL) {
		liste_param = cree_n_l_dec(opt_dec_variables->tete, opt_dec_variables->queue);
	}
	check_syntax_error(PARENTHESE_FERMANTE);
	show_and_consume();
	affiche_balise_fermante(__func__, trace_xml);
	return liste_param;
}

n_l_dec* optListeDecVariables()
{
	n_l_dec *opt_liste_dec_variables = NULL;
	n_l_dec *liste_dec_variables = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_listeDecVariables_, uniteCourante)) {
		liste_dec_variables = listeDecVariables();
		opt_liste_dec_variables = cree_n_l_dec(liste_dec_variables->tete, liste_dec_variables->queue);

		affiche_balise_fermante(__func__, trace_xml);
		return opt_liste_dec_variables;
	}
	if (est_suivant(_optListeDecVariables_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instruction()
{
	n_instr *instruction = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_instructionAffect_, uniteCourante)) {
		n_instr *instruction_affect = instructionAffect();
		n_var *var = instruction_affect->u.affecte_.var;
		n_exp *exp = instruction_affect->u.affecte_.exp;

		instruction = cree_n_instr_affect(var, exp);
	} else if (est_premier(_instructionBloc_, uniteCourante)) {
		n_instr *instruction_bloc = instructionBloc();
		n_l_instr *liste = instruction_bloc->u.liste;

		instruction = cree_n_instr_bloc(liste);
	} else if (est_premier(_instructionSi_, uniteCourante)) {
		n_instr *instruction_si = instructionSi();
		n_exp *exp = instruction_si->u.si_.test;
		n_instr *alors = instruction_si->u.si_.alors;
		n_instr *sinon = instruction_si->u.si_.sinon;

		instruction = cree_n_instr_si(exp, alors, sinon);
	} else if (est_premier(_instructionTantque_, uniteCourante)) {
		n_instr *instruction_tant_que = instructionTantque();
		n_exp *test = instruction_tant_que->u.tantque_.test;
		n_instr *faire = instruction_tant_que->u.tantque_.faire;

		instruction = cree_n_instr_tantque(test, faire);
	} else if (est_premier(_instructionAppel_, uniteCourante)) {
		n_instr *instruction_appel = instructionAppel();
		n_appel *appel = instruction_appel->u.appel;

		instruction = cree_n_instr_appel(appel);
	} else if (est_premier(_instructionRetour_, uniteCourante)) {
		n_instr *instruction_retour = instructionRetour();
		n_exp *expression = instruction_retour->u.retour_.expression;
		instruction = cree_n_instr_retour(expression);
	} else if (est_premier(_instructionEcriture_, uniteCourante)) {
		n_instr *instruction_ecriture = instructionEcriture();
		n_exp *exp = instruction_ecriture->u.ecrire_.expression;

		instruction = cree_n_instr_ecrire(exp);
	} else if (est_premier(_instructionVide_, uniteCourante)) {
		instructionVide();
		instruction = cree_n_instr_vide();
	} else if (est_premier(_instructionPour_, uniteCourante)) {
		n_instr *instruction_pour = instructionPour();

		n_instr *init = instruction_pour->u.pour_.init;
		n_exp *test = instruction_pour->u.pour_.test;
		n_instr *incr = instruction_pour->u.pour_.incr;
		n_instr *faire = instruction_pour->u.pour_.faire;

		instruction = cree_n_instr_pour(init, test, incr, faire);
	} else {
		erreur(__func__, __LINE__);
	}

	affiche_balise_fermante(__func__, trace_xml);
	return instruction;
}

n_instr* instructionAffect()
{
	n_instr *instruction_affect = NULL;
	n_var   *variable           = NULL;
	n_exp   *exp               = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_var_, uniteCourante)) {
		variable = var();
		check_syntax_error(EGAL);
		show_and_consume();

		if (est_premier(_expression_, uniteCourante)) {
			exp = expression();
			check_syntax_error(POINT_VIRGULE);
			show_and_consume();
			printf("Exp : %x\n", exp);
			instruction_affect = cree_n_instr_affect(variable, exp);
			affiche_balise_fermante(__func__, trace_xml);
			return instruction_affect;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionBloc()
{
	n_instr   *instruction_bloc   = NULL;
	n_l_instr *liste_instructions = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == ACCOLADE_OUVRANTE) {
		show_and_consume();
		if (est_premier(_listeInstructions_, uniteCourante) || est_suivant(_listeInstructions_, uniteCourante)) {
			liste_instructions = listeInstructions();
			check_syntax_error(ACCOLADE_FERMANTE);
			show_and_consume();
			instruction_bloc = cree_n_instr_bloc(liste_instructions);
			affiche_balise_fermante(__func__, trace_xml);
			return instruction_bloc;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_l_instr* listeInstructions()
{
	n_l_instr *liste_instructions        = NULL;
	n_instr   *instruc                   = NULL;
	n_l_instr *liste_instructions_concat = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_instruction_, uniteCourante)) {
		instruc = instruction();
		liste_instructions = listeInstructions();
		liste_instructions_concat = cree_n_l_instr(instruc, liste_instructions);

		affiche_balise_fermante(__func__, trace_xml);
		return liste_instructions_concat;
	}
	if (est_suivant(_listeInstructions_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionSi()
{
	n_exp   *exp               = NULL;
	n_instr *instruction_si    = NULL;
	n_instr *instruction_bloc  = NULL;
	n_instr *instruction_sinon = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(SI);
	show_and_consume();

	if (est_premier(_expression_, uniteCourante)) {
		exp = expression();
		check_syntax_error(ALORS);
		show_and_consume();
		if (est_premier(_instructionBloc_, uniteCourante)) {
			instruction_bloc  = instructionBloc();
			instruction_sinon = optSinon();
			instruction_si    = cree_n_instr_si(exp, instruction_bloc, instruction_sinon);

			affiche_balise_fermante(__func__, trace_xml);
			return instruction_si;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* optSinon()
{
	n_instr *opt_sinon = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);

	if(uniteCourante == SINON) {
		show_and_consume();
		if (est_premier(_instructionBloc_, uniteCourante)) {
			opt_sinon =  instructionBloc();

			affiche_balise_fermante(__func__, trace_xml);
			return opt_sinon;
		}
	}

	if (est_suivant(_optSinon_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionTantque()
{
	n_exp   *exp                 = NULL;
	n_instr *faire               = NULL;
	n_instr *instruction_tantque = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(TANTQUE);
	show_and_consume();

	if (est_premier(_expression_, uniteCourante)) {
		exp = expression();
		check_syntax_error(FAIRE);
		show_and_consume();
		if (est_premier(_instructionBloc_, uniteCourante)) {
			faire = instructionBloc();
			instruction_tantque = cree_n_instr_tantque(exp, faire);

			affiche_balise_fermante(__func__, trace_xml);
			return instruction_tantque;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionAppel()
{
	n_appel *appel = NULL;
	n_instr *instruction_appel = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);

	if (est_premier(_appelFct_, uniteCourante)) {
		appel = appelFct();
		check_syntax_error(POINT_VIRGULE);
		show_and_consume();
		instruction_appel = cree_n_instr_appel(appel);

		affiche_balise_fermante(__func__, trace_xml);
		return instruction_appel;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionRetour()
{
	n_exp   *exp                = NULL;
	n_instr *instruction_retour = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(RETOUR);
	show_and_consume();
	if (est_premier(_expression_, uniteCourante)) {
		exp = expression();
		check_syntax_error(POINT_VIRGULE);
		show_and_consume();
		instruction_retour = cree_n_instr_retour(exp);

		affiche_balise_fermante(__func__, trace_xml);
		return instruction_retour;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionEcriture()
{
	n_exp   *exp                  = NULL;
	n_instr *instruction_ecriture = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(ECRIRE);
	show_and_consume();
	check_syntax_error(PARENTHESE_OUVRANTE);
	show_and_consume();
	if (est_premier(_expression_, uniteCourante)) {
		exp = expression();
		check_syntax_error(PARENTHESE_FERMANTE);
		show_and_consume();
		check_syntax_error(POINT_VIRGULE);
		show_and_consume();
		instruction_ecriture = cree_n_instr_ecrire(exp);

		affiche_balise_fermante(__func__, trace_xml);
		return instruction_ecriture;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_instr* instructionVide()
{
	n_instr *instruction_vide = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(POINT_VIRGULE);
	show_and_consume();
	instruction_vide = cree_n_instr_vide();

	affiche_balise_fermante(__func__, trace_xml);
	return instruction_vide;
}

n_instr* instructionPour()
{
	n_instr *instruction = NULL;
	n_instr *init        = NULL;
	n_exp   *exp         = NULL;
	n_instr *incr        = NULL;
	n_instr *faire       = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	check_syntax_error(POUR);
	show_and_consume();
	if (est_premier(_instructionAffect_, uniteCourante)) {
		init = instructionAffect();
		if (est_premier(_expression_, uniteCourante)) {
			exp = expression();
			check_syntax_error(POINT_VIRGULE);
			show_and_consume();
			if (est_premier(_instructionAffect_, uniteCourante)) {
				incr = instructionAffect();
				check_syntax_error(FAIRE);
				show_and_consume();
				if (est_premier(_instructionBloc_, uniteCourante)) {
					faire = instructionBloc();
					instruction = cree_n_instr_pour(init, exp, incr, faire);

					affiche_balise_fermante(__func__, trace_xml);
					return instruction;
				}
			}
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* expression()
{
	n_exp *expresison = NULL;
	n_exp *conj = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_conjonction_, uniteCourante)) {
		conj = conjonction();
		expresison = expressionBis(conj);

		affiche_balise_fermante(__func__, trace_xml);
		return expresison;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* expressionBis(n_exp *heritage)
{
	n_exp* heritage_fils = NULL;
	n_exp* expr_bis = NULL;
	n_exp* conjonct = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == OU) {
		if (est_premier(_conjonction_, uniteCourante)) {
			conjonct = conjonction();
			heritage_fils = cree_n_exp_op(ou, heritage, conjonct);
			expr_bis = expressionBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return expr_bis;
		}
	}
	if (est_suivant(_expressionBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return heritage;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* conjonction()
{
	n_exp* conjonction_bis = NULL;
	n_exp* nega = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_negation_, uniteCourante)) {
		nega = negation();
		conjonction_bis = conjonctionBis(nega);

		affiche_balise_fermante(__func__, trace_xml);
		return conjonction_bis;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* conjonctionBis(n_exp *heritage)
{
	n_exp* neg = NULL;
	n_exp* conjonction_bis = NULL;
	n_exp* herite_fils = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == ET) {
		show_and_consume();
		if (est_premier(_negation_, uniteCourante)) {
			neg = negation();
			herite_fils = cree_n_exp_op(et, heritage, neg);
			conjonction_bis = conjonctionBis(herite_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return conjonction_bis;
		}
	}

	if (est_suivant(_conjonctionBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return heritage;
	}

	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* negation()
{
	n_exp* nega = NULL;
	n_exp* comp = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);

	if (uniteCourante == NON) {
		show_and_consume();
		comp = comparaison();
		nega = cree_n_exp_op(non, comp, NULL);

		affiche_balise_fermante(__func__, trace_xml);
		return nega;
	}

	if (est_premier(_comparaison_, uniteCourante)) {
		nega = comparaison();

		affiche_balise_fermante(__func__, trace_xml);
		return nega;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* comparaison()
{
	n_exp* exp_arith = NULL;
	n_exp* comparaison_bis = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_expArith_, uniteCourante)) {
		exp_arith = expArith();
		comparaison_bis = comparaisonBis(exp_arith);

		affiche_balise_fermante(__func__, trace_xml);
		return comparaison_bis;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* comparaisonBis(n_exp *heritage)
{
	n_exp* exp_arith = NULL;
	n_exp* comparaison_bis = NULL;
	n_exp* heritage_fils = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == EGAL) {
		show_and_consume();
		if (est_premier(_expArith_, uniteCourante)) {
			exp_arith = expArith();
			heritage_fils = cree_n_exp_op(egal, heritage, exp_arith);
			comparaison_bis = comparaisonBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return comparaison_bis;

		}
	} else if (uniteCourante == INFERIEUR) {
		show_and_consume();
		if (est_premier(_expArith_, uniteCourante)) {
			exp_arith = expArith();
			heritage_fils = cree_n_exp_op(inf, heritage, exp_arith);
			comparaison_bis = comparaisonBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return comparaison_bis;
		}
	}

	if (est_suivant(_comparaisonBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return heritage;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* expArith()
{
	n_exp* terme_ = NULL;
	n_exp* exp_arith_bis = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_terme_, uniteCourante)) {
		terme_ = terme();
		exp_arith_bis = expArithBis(terme_);
		affiche_balise_fermante(__func__, trace_xml);
		return exp_arith_bis;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* expArithBis(n_exp *heritage)
{
	n_exp* exp_arith_bis = NULL;
	n_exp* terme2 = NULL;
	n_exp* heritage_fils = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == PLUS) {
		show_and_consume();
		if (est_premier(_terme_, uniteCourante)) {
			terme2 = terme();
			heritage_fils = cree_n_exp_op(plus, heritage, terme2);
			exp_arith_bis = expArithBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return exp_arith_bis;
		}
	}
	if (uniteCourante == MOINS) {
		show_and_consume();
		if (est_premier(_terme_, uniteCourante)) {
			terme2 = terme();
			heritage_fils = cree_n_exp_op(moins, heritage, terme2);
			exp_arith_bis = expArithBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return exp_arith_bis;
		}
	}
	if (est_suivant(_expArithBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return heritage;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* terme()
{
	n_exp* fact = NULL;
	n_exp* terme_bis = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_facteur_, uniteCourante)) {
		fact = facteur();
		terme_bis = termeBis(fact);
		affiche_balise_fermante(__func__, trace_xml);
		return terme_bis;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* termeBis(n_exp* heritage)
{
	n_exp* terme_bis = NULL;
	n_exp* fact2 = NULL;
	n_exp* heritage_fils = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == FOIS) {
		show_and_consume();
		if (est_premier(_facteur_, uniteCourante)) {
			fact2 = facteur();
			heritage_fils = cree_n_exp_op(fois, heritage, fact2);
			terme_bis = termeBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return terme_bis;
		}
	}
	if (uniteCourante == DIVISE) {
		show_and_consume();
		if (est_premier(_facteur_, uniteCourante)) {
			fact2 = facteur();
			heritage_fils = cree_n_exp_op(divise, heritage, fact2);
			terme_bis = termeBis(heritage_fils);

			affiche_balise_fermante(__func__, trace_xml);
			return terme_bis;
		}
	}
	if (est_suivant(_termeBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return heritage;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* facteur()
{
	n_exp *fact = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == PARENTHESE_OUVRANTE) {
		show_and_consume();
		if (est_premier(_expression_, uniteCourante)) {
			fact = expression();
			check_syntax_error(PARENTHESE_FERMANTE);
			show_and_consume();
			affiche_balise_fermante(__func__, trace_xml);
			return fact;
		}
	}
	if (uniteCourante == NOMBRE) {
		char nombre[YYTEXT_MAX];
		strcpy(nombre, yytext);
		show_and_consume();
		int result = atoi(nombre);
		fact = cree_n_exp_entier(result);

		affiche_balise_fermante(__func__, trace_xml);
		return fact;
	}
	if (est_premier(_appelFct_, uniteCourante)) {
		n_appel *appel_fct = appelFct();
		fact = cree_n_exp_appel(appel_fct);
		affiche_balise_fermante(__func__, trace_xml);
		return fact;
	}
	if (est_premier(_var_, uniteCourante)) {
		n_var *var_ = var();
		fact = cree_n_exp_var(var_);
		affiche_balise_fermante(__func__, trace_xml);
		return fact;
	}
	if (uniteCourante == LIRE) {
		affiche_unite_element(uniteCourante);
		uniteCourante = yylex();
		if (uniteCourante == PARENTHESE_OUVRANTE) {
			affiche_unite_element(uniteCourante);
			uniteCourante = yylex();
			check_syntax_error(PARENTHESE_FERMANTE);
			show_and_consume(PARENTHESE_FERMANTE);

			fact = cree_n_exp_lire();

			affiche_balise_fermante(__func__, trace_xml);
			return fact;
		}
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_var* var()
{
	affiche_balise_ouvrante(__func__, trace_xml);
	if(uniteCourante == ID_VAR) {
		char var_name[YYTEXT_MAX];
		strcpy(var_name, yytext);
		show_and_consume();
		n_exp* indice = optIndice();
		n_var* var = NULL;
		if(indice == NULL)
			var = cree_n_var_simple(var_name);
		else
			var = cree_n_var_indicee(var_name, indice);
		affiche_balise_fermante(__func__, trace_xml);
		return var;
	}
	if (est_suivant(_var_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_exp* optIndice()
{
	n_exp* indice = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (uniteCourante == CROCHET_OUVRANT) {
		show_and_consume();
		if (est_premier(_expression_, uniteCourante)) {
			indice = expression();
			if (uniteCourante == CROCHET_FERMANT) {
				show_and_consume();
				affiche_balise_fermante(__func__, trace_xml);
				return indice;
			}
		}
	}
	if (est_suivant(_optIndice_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_appel* appelFct() {
	n_appel *appel_fonction = NULL;
	n_l_exp* args = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	char id_function[YYTEXT_MAX];
	strcpy(id_function, yytext);
	check_syntax_error(ID_FCT);
	show_and_consume();
	check_syntax_error(PARENTHESE_OUVRANTE);
	show_and_consume();
	args = listeExpressions();
	check_syntax_error(PARENTHESE_FERMANTE);
	show_and_consume();
	appel_fonction = cree_n_appel(id_function, args);
	affiche_balise_fermante(__func__, trace_xml);
	return appel_fonction;
}

n_l_exp* listeExpressions()
{
	n_l_exp* liste_expressions = NULL;
	n_exp *exp = NULL;
	n_l_exp *liste_expressions_bis = NULL;
	affiche_balise_ouvrante(__func__, trace_xml);
	if (est_premier(_expression_, uniteCourante)) {
		exp = expression();
		liste_expressions_bis = listeExpressionsBis();

		liste_expressions = cree_n_l_exp(exp, liste_expressions_bis);

		affiche_balise_fermante(__func__, trace_xml);
		return liste_expressions;
	}
	if (est_suivant(_listeExpressions_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

n_l_exp* listeExpressionsBis()
{
	n_l_exp* liste_expressions_bis_result = NULL;
	n_exp* exp = NULL;
	n_l_exp* liste_expressions_bis = NULL;

	affiche_balise_ouvrante(__func__, trace_xml);

	if (uniteCourante == VIRGULE) {
		show_and_consume();
		if (est_premier(_expression_, uniteCourante)) {
			exp = expression();
			liste_expressions_bis = listeExpressionsBis();

			liste_expressions_bis_result = cree_n_l_exp(exp, liste_expressions_bis);

			affiche_balise_fermante(__func__, trace_xml);
			return liste_expressions_bis_result;
		}
	}
	if (est_suivant(_listeExpressionsBis_, uniteCourante)) {
		affiche_balise_fermante(__func__, trace_xml);
		return NULL;
	}
	erreur(__func__, __LINE__);
	return NULL;
}

void affiche_grille_premiers_suivants_ll_table(void)
{
	for (int i = 0; i < NB_NON_TERMINAUX; i++) {
		printf("%-24s |", symboles_non_terminaux_name[i]);
		for (int j = 0; j <= NB_TERMINAUX; j++) {
			putchar(premiers[i][j] ? 'X' : ' ');
		}
		putchar('|');
		putchar('|');
		for (int j = 0; j <= NB_TERMINAUX; j++) {
			putchar(suivants[i][j] ? 'X' : ' ');
		}
		putchar('|');
		putchar('|');
		for (int j = 0; j <= NB_TERMINAUX; j++) {
			putchar(ll_table[i][j] ? 'X' : ' ');
		}
		puts("|");
	}
	puts("---");
}

void analyser_syntaxe(FILE *yyin)
{
	initialise_premiers();
	initialise_suivants();
	initialise_ll_table();
	trace_xml = 1;
	uniteCourante = yylex();
	n_prog* prog =  programme();
	affiche_n_prog(prog);
}
