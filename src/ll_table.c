#include "ll_table.h"
#include "premiers.h"
#include "suivants.h"
#include "symboles.h"

void initialise_ll_table(void) {
	for(int i=0; i <= NB_NON_TERMINAUX; i++) {
		for(int j=1; j <= NB_TERMINAUX; j++) {
			ll_table[i][j] = premiers[i][j];
			if(premiers[i][EPSILON] && suivants[i][j]) {
				ll_table[i][j] = suivants[i][j];
			}
		}
	}
}

int is_ll_transition(int non_terminal, int terminal) {
	return ll_table[non_terminal][terminal];
}