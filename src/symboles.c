#include "symboles.h"

const char *symboles_terminaux_name[] = {
#define X_V(a, b) X(a)
#define X(a) #a,
#include "symboles_terminaux.def"
#undef X
#undef X_V
};

const char *symboles_non_terminaux_name[] = {
#define X(a) #a,
#include "symboles_non_terminaux.def"
#undef X
};