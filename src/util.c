#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "symboles.h"
#include "analyseur_syntaxique.h"
#include "analyseur_lexical.h"

/*-------------------------------------------------------------------------*/

extern int nb_ligne;

static int indent_xml = 0;
static int indent_step = 1; // set to 0 for no indentation

// Static functions prototypes
static void indent();
static void affiche_xml_texte( char *texte_ );

/*-------------------------------------------------------------------------*/

void erreur(const char *message, const int ligne)
{
    fprintf (stderr, "Ligne %d (%d): ", ligne, nb_ligne);
    fprintf (stderr, "%s\n", message);
    fprintf (stderr, "Token : %s\n", symboles_terminaux_name[uniteCourante]);
    fprintf(stderr, "Value : %s\n", yytext);
    exit(1);
}

/*-------------------------------------------------------------------------*/

char *duplique_chaine(char *src)
{
    char *dest = malloc(sizeof(char) * strlen(src));
    strcpy(dest, src);
    return dest;
}

/*-------------------------------------------------------------------------*/
void affiche_balise_ouvrante(const char *fct_, int trace_xml)
{
    if( trace_xml )
    {
        indent();
        indent_xml += indent_step;
        fprintf (stdout, "<%s>\n", fct_);
    }
}

/*-------------------------------------------------------------------------*/

void affiche_balise_fermante(const char *fct_, int trace_xml)
{
    if(trace_xml)
    {
        indent_xml -= indent_step;
        indent();
        fprintf (stdout, "</%s>\n", fct_);
    }
}

/*-------------------------------------------------------------------------*/

void affiche_texte(char *texte_, int trace_xml)
{
    if(trace_xml)
    {
        indent();
        fprintf (stdout, "%s\n", texte_);
    }
}

/*-------------------------------------------------------------------------*/

void affiche_element(char *fct_, char *texte_, int trace_xml)
{
    if(trace_xml)
    {
        indent();
        fprintf (stdout, "<%s>", fct_ );
        affiche_xml_texte( texte_ );
        fprintf (stdout, "</%s>\n", fct_ );
    }
}

/*******************************************************************************
* Fonction auxiliaire appelée par l'analyseur syntaxique tout simplement pour
* afficher des messages d'erreur et l'arbre XML
******************************************************************************/
void nom_token(int token, char *nom, char *valeur)
{
    if( token == ID_VAR )
    {
        strcpy( nom, "id_variable" );
        strcpy( valeur, yytext );
    }
    else if( token == ID_FCT )
    {
        strcpy( nom, "id_fonction" );
        strcpy( valeur, yytext );
    }
    else if( token == NOMBRE )
    {
        strcpy( nom, "nombre" );
        strcpy( valeur, yytext );
    }
    else if( token == POINT_VIRGULE || token == PLUS || token == MOINS || token == FOIS
        || token == DIVISE || token == PARENTHESE_OUVRANTE || token == PARENTHESE_FERMANTE
        || token == CROCHET_OUVRANT || token == CROCHET_FERMANT || token == ACCOLADE_OUVRANTE
        || token == ACCOLADE_FERMANTE || token == EGAL || token == INFERIEUR || token == VIRGULE
        || token == OU || token == NON || token == ET)
    {
        strcpy( nom, "symbole" );
        strcpy( valeur, symboles_terminaux_name[token]);
    }
    else if(token == FIN) {
        strcpy( nom, "symbole" );
        strcpy( valeur, symboles_terminaux_name[token]);
    }
    else if(token < 0)
    {
        strcpy( nom, "erreur" );
        strcpy( valeur, symboles_terminaux_name[NB_TERMINAUX -1 + (-token)] );
    }
    else {
        strcpy( nom, "mot_clef" );
        for (int i = 0; i < strlen(symboles_terminaux_name[token]); ++i)
        {
            valeur[i] = tolower(symboles_terminaux_name[token][i]);
        }
        valeur[strlen(symboles_terminaux_name[token])] = '\0';
    }
}


void affiche_unite_element(int unite) {
    char buffer_name[256] = "";
    char buffer_value[256] = "";
    nom_token(unite, buffer_name, buffer_value);
    affiche_element(buffer_name, buffer_value, trace_xml);
}

//////////////////////////////////////////////////////////////////////////////
// STATIC FUNCTIONS
//////////////////////////////////////////////////////////////////////////////

static void indent()
{
    int i;
    for( i = 0; i < indent_xml; i++ )
    {
        printf( "  " );
    }
}

/*-------------------------------------------------------------------------*/

static void affiche_xml_texte( char *texte_ )
{
    int i = 0;
    while( texte_[ i ] != '\0' )
    {
        if( texte_[ i ] == '<' )
        {
            fprintf( stdout, "&lt;" );
        }
        else if( texte_[ i ] == '>' )
        {
            fprintf( stdout, "&gt;" );
        }
        else if( texte_[ i ] == '&' )
        {
            fprintf( stdout, "&amp;" );
        }
        else {
            putchar( texte_[i] );
        }
        i++;
    }
}
